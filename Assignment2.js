//So it works, someone forgot to ask whoWins() to return the value
		var gamePieces=[];
		var usrInput;
		var uC;	//stores user choice
		var i;	
		var cC;	//stores computer choice
		var outcome; //stores game result
		var result; //stores whoWins() return

function getRandomGamePieces()
    {		//initialize array gamePieces
    	    gamePieces = ["rock","paper","scissors","dynamite"];
       		
       		//user input, pass value to usrInput
        	usrInput = prompt ("Please enter 'rock','paper','scissors' or 'dynamite'");

        	//convert usrInput value to lowecase and pass to var uC 
        	uC= usrInput.toLowerCase();

            // creating random number
        	i = Math.floor(Math.random()*gamePieces.length);
        	//alert (i);

        	//assign value to cC var to store computer choice
        	
            //cC = gamePieces[i]; could be used instead of switch
			switch(i)
		     {
	            case 0:   
	                cC = "rock";

	                break;
	            case 1:   
	                cC = "paper";
	                
	                break;
	            case 2:   
	                cC = "scissors";
	                 
	                break;
	            case 3:
	                cC = "dynamite";
	                
	                break;
	         }
	   
	   //assign whoWins() output to var result and print into #results with JQ
	 result=whoWins(cC,uC);

	 $(document).ready(function()
        {
        $("#results").text("Computer chose "+ cC + ". " + result);
 
        });
}

	function whoWins(cC, uC)
		{	//alert ("cC: "+cC);
			//alert ("uC: "+uC);
			//Computer wins if..., else if...
	       if (cC =="rock" && uC=="scissors")
	         {
			 	outcome="Computer won!";
	         }
			else if (cC =="paper" && uC=="rock")
	         {
	         	outcome="Computer won!";
	         }
	         //combining two options for uC with or
			else if (cC=="scissors" && (uC=="paper" || uC=="dynamite"))
	         {
	         	outcome="Computer won!";
	         }
			else if (cC=="dynamite" && (uC=="paper" || uC=="rock"))
	         {
	         	outcome="Computer won!";
	         }
	         //if equal
			else if(cC==uC)
	        {
	        	outcome="Tie game!"
	        }
	        //if everything so far is not true, then user wins
	        else {
	            outcome="You won!"
	        }
	        //Same concept with switch()
	        /*switch (cC, uC){
	        	case "rock", "scissors":
	        	case "paper", "rock":
	        	case "scissors", "paper":
	        	case "scissors", "dynamite":
	        	case "dynamite", "paper":
	        	case "dynamite", "rock":
	        		outcome = "Computer wins!";
	        		break;
	        	case "rock", "rock":
	        	case "paper", "paper":
	        	case "scissors", "scissors":
	        	case "dynamite", "dynamite":
	        		outcome = "Tie";
	        		break;
	        	default:
	        		outcome = "You won!";
	        		break;
	        }*/

	        return outcome;
	     }




