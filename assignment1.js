
//Create function for validation called on click of "Submit" button
function validateForm(){

    //assign variables to forms to limit repeat
      var a = document.getElementById('txtName');
      var b = document.getElementById('txtEmail');
      var c = document.getElementById('cboProv');

//Validation for empty Name input, with alert message and focus on the form if no user input
    if(a.value==""){
        alert('Please enter Your Name');
        a.focus();
        return;
    }
//Validation for empty Email input, with alert message and focus on the form if no user input
    if(b.value==""){
        alert('Please enter valid email');
        b.focus();
        return;
    }
//Validation for Select Box, with alert message and focus on the form if no user selection = "-Select-"
    if(c.options[c.selectedIndex].value==""){
        alert('Please select province');
        c.focus();
        return;
    }
//Confirmation message for successfully collected data
    else {
        alert('Thanks for cooperating with CIA!');
    }
  } 
//Function on body load  
function loadProvinces(){

//Creating of heading and input text forms
      document.write("<h1>Personal Information</h1>" + "</br>");
      document.write("Name: <input id='txtName' name='txtName' type='textbox'>" + "<br/>" + "<br/>");
      document.write("Email: <input id='txtEmail' name='txtEmail' type='email'>" + "<br/>" + "<br/>");

//Creating array with Provinces names

     	var provArray = ["Nunavut", "Quebec", "Northwest Territories", "Ontario", "British Columbia", "Alberta", "Saskatchwan", "Manitoba", "Yukon", "Newfoundland and Labrador", "New Brinswick", "Nova Scotia", "Prince Edward Island"];

//Creating selection box and default option "-Selec-""
  		document.write("Province: <select id='cboProv' name='cboProv'>");
  		document.write("<option name='99' value=''>-Select-</option>");

//Creating other options of sel.box with "for" loop

   	for (arIndex=0; arIndex<provArray.length; arIndex++){
   		  document.write("<option name='"+ arIndex + "' value=" + provArray[arIndex] + ">" + provArray[arIndex] + "</option>");
    }
//Create select box closing tag
     	document.write("</select>" + "</br>" + "</br>");

//creating "Submit" button with validation function onclick event
     	document.write("<button name='submit' onclick='validateForm();'>submit</button>");
}

